/**
 * Created by zhaozailin on 2017/4/4.
 */
var play = (function () {
    var init = function ($obj, totalTime) {
        var curTime = 0;
        var timer = null;

        // 初始化总时间
        var $progress = $('.pub-progress');
        $progress.find('label').eq(1).html(play.secondsToBeginTime(totalTime));

        $obj.click(function () {

            // 如果是暂停就播放
            if ($obj.hasClass('icon-kaishi')) {

                // 改变icon
                $obj.removeClass('icon-kaishi').addClass('icon-zanting');

                timer = setInterval(function () {
                    curTime++;

                    if (curTime > totalTime) {
                        clearInterval(timer);
                        $('.pub-flag').css('left', '0px');
                        curTime=0;
                        $obj.removeClass('icon-zanting').addClass('icon-kaishi');
                    }

                    var curPosi = curTime / totalTime * 223;
                    $('.pub-flag').css('left', curPosi + 'px');
                    $progress.find('label').eq(0).html(play.secondsToBeginTime(curTime));
                    //$progress.find('label').eq(1).html(play.secondsToEndTime(totalTime, curTime));
                }, 1000);
            }

            // 如果是播放就暂停
            else {

                // 改变icon
                $obj.removeClass('icon-zanting').addClass('icon-kaishi');

                clearInterval(timer);
            }
        });
    };

    // 转化秒数为开始时间
    var secondsToBeginTime = function (seconds) {
        var hours = parseInt(seconds / 3600);
        hours = hours < 10 ? ('0' + hours) : hours;
        var minutes = parseInt((seconds % 3600) / 60);
        minutes = minutes < 10 ? ('0' + minutes) : minutes;
        var seconds = parseInt(seconds % 60);
        seconds = seconds < 10 ? ('0' + seconds) : seconds;
        return hours + ':' + minutes + ':' + seconds;
    };

    // 转化秒数为结束时间
    var secondsToEndTime = function (totalSeconds, seconds) {
        seconds = totalSeconds - seconds;
        var hours = parseInt(seconds / 3600);
        hours = hours < 10 ? ('0' + hours) : hours;
        var minutes = parseInt((seconds % 3600) / 60);
        minutes = minutes < 10 ? ('0' + minutes) : minutes;
        var seconds = parseInt(seconds % 60);
        seconds = seconds < 10 ? ('0' + seconds) : seconds;
        return hours + ':' + minutes + ':' + seconds;
    };

    return {
        init: init,
        secondsToBeginTime: secondsToBeginTime,
        secondsToEndTime: secondsToEndTime
    };
})();